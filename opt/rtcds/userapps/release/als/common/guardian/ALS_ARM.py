import lscparams
from guardian import GuardState, GuardStateDecorator
import ISC_library
import gpstime
from cdsutils.getdata import getdata
import cdsutils
import time
from timeout_utils import call_with_timeout
import numpy as np
import awg
import alsArmconfig

##################################################
# Functions and ALS-specific decorators
##################################################


def gen_CHECK_CRYSTAL_FREQ(arm):
    """
    #SED note, this comment doesn't make sense to me, the crystal temp shouldn't need to be in a particular range.  but maybe the comment is just written in a misleading way and what is being checked is the BEAT_FREQUENCY
    This state will check a few channels to determine if the crystal freq is within range 38.9068 < X < 40.3068
    Check if theres a FIBR lock flag, then if theres a freq error and finally it check if the beatnote's
     far out of tolerance
    """

    class CHECK_CRYSTAL_FREQ(GuardState):
        request = False
        redirect = False

        def main(self):
            self.crystal_freq = [100, -100, -150]  # It often finds it at 100 or -100
            self.timer['Cryst_freq_loop'] = 190  # Give this state a max time to search
            self.timer['Cryst_freq_search'] = 0  # Give it some time to search
            self.timer["Crystal_fixed"] = 0
            self.fixed_flag = False
            self.crys_index = 0
            log("Turning on autolocker and inputting frequency values to search around")
            ezca['ALS-' + arm + '_FIBR_LOCK_LOGIC_ENABLE'] = 0  # Turn off automation, not sure if needed
            ezca['ALS-' + arm + '_FIBR_LOCK_LOGIC_FORCE'] = 1  # Turn on force, which ignores the error signal

        def run(self):
            # Beat frequency is the most important check
            if not 38.9068 < ezca['ALS-' + arm + '_FIBR_LOCK_BEAT_FREQUENCY'] < 40.3068 and \
                    abs(ezca['ALS-' + arm + '_FIBR_LOCK_BEAT_FREQUENCYERROR']) >= 100 and \
                    ezca['ALS-' + arm + '_FIBR_LOCK_ERROR_FLAG'] == 1:  # Does this error flag always come up?
                # FreqError is nominally close to zero
                log("Crystal Frequency out of tolerance")
                # When in error Y goes to a large neg and X goes t a large pos value, Order of 10^8

            elif 38.9068 < ezca['ALS-' + arm + '_FIBR_LOCK_BEAT_FREQUENCY'] < 40.3068 and \
                    abs(ezca['ALS-' + arm + '_FIBR_LOCK_BEAT_FREQUENCYERROR']) <= 100 and \
                    ezca['ALS-' + arm + '_FIBR_LOCK_ERROR_FLAG'] == 0:  # If the issue is fixed
                log("Fixed?")
                self.fixed_flag = True  # If the else statement isn't catching it? Try a flag?
                ezca['ALS-' + arm + '_FIBR_LOCK_LOGIC_FORCE'] = 0 # Turn off force
                return True  # Should jump back into locking after being fixed

            if self.timer['Cryst_freq_search']:
                if self.crys_index > len(
                        self.crystal_freq):  # If you've gone through the whole list and not fixed the issue
                    ezca['ALS-' + arm + '_FIBR_LOCK_LOGIC_ENABLE'] = 1  # Turning back on automation
                    ezca['ALS-' + arm + '_FIBR_LOCK_LOGIC_FORCE'] = 0  # Turning off force
                    return 'FAULT'
                time.sleep(5)  # Give it some extra time to settle?
                try:  # For some reason it got an index error on 11/07/23, so I'm adding this and another timer
                    ezca['ALS-' + arm + '_LASER_HEAD_CRYSTALFREQUENCY'] = self.crystal_freq[
                        self.crys_index]  # Search through the freq list
                    self.timer['Cryst_freq_search'] = 80
                    self.crys_index += 1
                except IndexError:
                    print("Searched through the whole list of frequencies")
                    ezca['ALS-' + arm + '_FIBR_LOCK_LOGIC_FORCE'] = 0 # Turn off force
                    return 'FAULT'

                return False

                # The error flag drops before the error signal is low enough and freq is fully good, but they're on the way
            #elif not 38.9068 < ezca['ALS-' + arm + '_FIBR_LOCK_BEAT_FREQUENCY'] < 40.3068 and \
             #       abs(ezca['ALS-' + arm + '_FIBR_LOCK_BEAT_FREQUENCYERROR']) >= 100:
              #  log("On the right path? Giving it some time to settle")
               # self.timer['Crystal_freq_search'] = 45  # Give it some time for the frequency to be found

           # if self.fixed_flag:  # This should be the fixed case, take us back to FAULT where it will see if its good
            #    # and bring it back to the requested state
             #   ezca['ALS-' + arm + '_FIBR_LOCK_LOGIC_ENABLE'] = 1  # Turning back on automation
              #  ezca['ALS-' + arm + '_FIBR_LOCK_LOGIC_FORCE'] = 0  # Turning off the autolocker
               # return 'FAULT'

    return CHECK_CRYSTAL_FREQ


def fault_checker_gen(arm):
    class FaultChecker(GuardStateDecorator):
        def pre_exec(self):
            if is_fault(arm):
                log(ezca['ALS-'+arm+'_LOCK_ERROR_MSG'])
                log(ezca['ALS-'+arm+'_REFL_LOCK_ERROR_MSG'])
                log(ezca['ALS-'+arm+'_FIBR_LOCK_ERROR_MSG'])
                return 'FAULT'
    return FaultChecker


def is_fault(arm):
    message = []
    flag = False
    if ezca['ALS-'+arm+'_LOCK_ERROR_FLAG']:
        flag = True
        notify(ezca['ALS-'+arm+'_LOCK_ERROR_MSG'])
        if ezca['ALS-'+arm+'_REFL_LOCK_ERROR_FLAG']:
            notify(ezca['ALS-'+arm+'_REFL_LOCK_ERROR_MSG'])
        if ezca['ALS-'+arm+'_FIBR_LOCK_ERROR_FLAG']:
            notify(ezca['ALS-'+arm+'_FIBR_LOCK_ERROR_MSG'])
            if ezca['ALS-'+arm+'_FIBR_LOCK_ERROR_MSG'] == 'VCO':
                ezca['ALS-Y_VCO_CONTROLS_CLEARINT'] = 1
    # Not sure atm if the error flag is always True if the trig mon is 0
    if ezca['ALS-{}_PZT_TRIG_MON'.format(arm)] == 0:
        flag = True
        notify('PZT trig servo not on')
    return flag



def avg_convergence_checker(channels_dict, delay=3, duration=-30):
    """Average the channels given in a dictionary and if all are
    below threshold, return True.

    channels_dict - Python dictionary with the channels as
    the keys and their threshold as the value. 

    delay - Integer number of seconds from end of data block till now.
    (Some delay is recommended to avoid data retrieval errors.)

    duration - Length of time to take data.
    """
    channels = list(channels_dict.keys())
    # Add in the ifo prefix if it isn't there already (cheap way for both ifos)
    #if not channels[0][1:3] == '1:':
    #    ifo = os.getenv('IFO')
    #    channels = ['{}:{}'.format(ifo, chan) for chan in channels]
    # # Try for data, sometimes it errors with a RunTimeError when it cant get it.
    # counter = 0
    # while counter < 4:
    #     try:
    #         data_all = getdata(channels, duration, start=int(gpstime.gpsnow()) - duration - delay)
    #         break
    #     except RuntimeError:
    #         log('Encountered RuntimeError when getting data, trying again')
    #         counter += 1
    #         if counter >= 4:
    #             log('Unable to get data after 3 tries. Returning False.')
    #             return False
    #         continue

    # unconverged = []
    # # Add all channels that arent below thresh to the list
    # for dat in data_all:
    #     avg = sum(dat.data) / len(dat.data)
    #     #print(dat.channel, avg, channels_dict[dat.channel])
    #     if abs(avg) > channels_dict[dat.channel]:
    #         unconverged.append(dat.channel)

    # FIXME: use this instead:
    unconverged = []
    avgs = call_with_timeout(cdsutils.avg, duration, channels)
    for channel, avg in zip(channels, avgs):
        if abs(avg) > channels_dict[channel]:
            unconverged.append(channel)

    if unconverged:
        log('Waiting for channels to converge: {}'.format(unconverged))
        return False
    else:
        return True


#################################################
# STATES:
#################################################

def gen_INIT(arm):
    class INIT(GuardState):

        @fault_checker_gen(arm)
        def main(self):
            if ezca['ALS-'+self.arm+'_REFL_LOCK_STATE'] == 2:
                # If the arm is locked, classify which state it's in
                if ezca['ALS-C_LOCK_REQUEST'+self.arm] == 'End Locked':
                    return 'INITIAL_ALIGNMENT'
                elif ezca['ALS-C_LOCK_REQUEST'+self.arm] == 'Tidal Engaged':
                    return 'LOCKED_W_SLOW'

    return INIT


def gen_FAULT(arm):
    class FAULT(GuardState):
        request = False
        redirect = False

        def main(self):
            #turn off the WFS
            ezca['ALS-'+self.arm+'_WFS_SWITCH'] = 0

        def run(self):
            log('checking fault')
            #  Crystal frequency fault check
            if abs(ezca['ALS-' + arm + '_FIBR_LOCK_BEAT_FREQUENCYERROR']) > 100 and not (38.9068 < ezca[
                'ALS-' + arm + '_FIBR_LOCK_BEAT_FREQUENCY'] < 40.3068):
                log('Crystal frequency error')
                return 'CHECK_CRYSTAL_FREQ'
            if is_fault(arm):
                if ezca.read('GRD-ALS_%sARM_REQUEST'%self.arm, as_string=True) == 'UNLOCKED':
                    # If in fault (ex. because green is shuttered) but want unlocked, jump to unlocked rather than stay stuck
                    return 'UNLOCKED'
                else:
                    # otherwise, stay stuck in fault until things are better
                    return
            else:
                return True

    return FAULT


def gen_UNLOCKED(arm):
    class UNLOCKED(GuardState):
        goto=True

        def main(self):
            ezca['ALS-'+self.arm+'_LOCK_ENABLE'] = 1
            ezca['ALS-C_LOCK_REQUEST'+self.arm] = 'PLL Locked'
            for py in ['P','Y']:
                ezca.switch('ALS-%s_WFS_DOF_1_%s'%(self.arm,py),'INPUT','OFF')
                ezca.switch('ALS-%s_WFS_DOF_2_%s'%(self.arm,py),'INPUT','OFF')
                ezca.switch('ALS-%s_WFS_DOF_3_%s'%(self.arm,py),'INPUT','OFF')
                ezca['ALS-%s_WFS_DOF_1_%s_RSET'%(self.arm,py)] = 2
                ezca['ALS-%s_WFS_DOF_2_%s_RSET'%(self.arm,py)] = 2
                ezca['ALS-%s_WFS_DOF_3_%s_RSET'%(self.arm,py)] = 2
                ezca['SUS-ITM%s_M0_LOCK_%s_RSET'%(self.arm,py)] = 2
                ezca['SUS-ETM%s_M0_LOCK_%s_RSET'%(self.arm,py)] = 2
                ezca['SUS-TMS%s_M1_LOCK_%s_RSET'%(self.arm,py)] = 2

        def run(self):
            #disable PDH
            ezca['ALS-'+self.arm+'_REFL_SERVO_IN1EN'] = 0
            return True

    return UNLOCKED


def gen_INCREASE_FLASHES(arm):
    class INCREASE_FLASHES(GuardState):
        """Move in a direction and determine if the flashes are better or worse.
        If worse, go the other way. Do this for both P & Y, with the hopes that
        we can get the arm power high enough to catch when we try to lock again.

        * Dont forget a timeout

        To fix:
          * On second iteration, we again get the point that we had in the last
            iter. Takes 20sec longer
        """
        redirect = False
        @fault_checker_gen(arm)
        def main(self):
            self.gen_chan = gen_chan = 'SUS-{sus}{arm}_{stage}_{bank}_{dof}{suffix}'
            # This needs a better place to live
            def restore_offsets():
                for sus in self.sus:
                    for dof in ['P', 'Y']:
                        #ezca['SUS-{}{}_{}_TEST_{}_OFFSET'.format(sus, arm, self.levels[sus], dof)] = self.orig_test_offsets[sus][dof]
                        ezca['SUS-{}{}_{}_TEST_{}_OFFSET'.format(sus, arm, self.levels[sus], dof)] = lscparams.misalign_offsets[sus][arm][dof]
                        ezca.get_LIGOFilter('SUS-{}{}_{}_TEST_{}'.format(sus, arm, self.levels[sus], dof)).switch_off('OFFSET')
            self.restore_offsets = restore_offsets
            # Constants for this state
            self.pwr_thresh = {'X': 0.8,
                               'Y': 0.9 #was 0.9 but y arm is bad 20240305
            }
            self.data_dur = 20
            self.sus = ['ETM', 'TMS']
            self.levels = {'ETM': 'M0', 'TMS': 'M1'}
            # FIXME: Change this when we get more data. Jim was looking into it.
            self.search_offsets = {'ETM': [-1.0, -0.5, 0.0, 0.5, 1.0],
                                   'TMS': [-0.8, -0.4, 0.0, 0.4, 0.8]
            }
            self.offset_list_counter = 0
            self.cur_dof = 'P'
            self.cur_sus = self.sus[0]
            self.stage = self.levels[self.cur_sus]
            # Set up timers
            self.timer['offset_tramp'] = 0
            self.flashes_high_enough = False

            # Initialize emplty dicts
            self.orig_test_offsets = {}
            self.offset_list = {}
            self.offset_data = {}
            for sus in self.sus:
                # Save initial values
                self.orig_test_offsets[sus] = {dof: ezca['SUS-{}{}_{}_TEST_{}_OFFSET'.format(sus, arm, self.levels[sus], dof)] \
                                               for dof in ['P', 'Y']}
                # Zero and turn on Offset
                for py in ['P', 'Y']:
                    ezca['SUS-{}{}_{}_TEST_{}_OFFSET'.format(sus, arm, self.levels[sus], py)] = 0
                    ezca.get_LIGOFilter('SUS-{}{}_{}_TEST_{}'.format(sus, arm, self.levels[sus], py)).switch_on('OFFSET')
                # Dummy list to start. Start with five but we may reduce it later
                self.offset_list[sus] = {dof: self.search_offsets[sus] for dof in ['P', 'Y']}
                # FIXME: Do we have a good number for this?
                self.off_tramp = ezca['SUS-{}{}_{}_TEST_{}_TRAMP'.format(sus, arm, self.levels[sus], self.cur_dof)]
                # A place to store data. Each dof will hold a list of 2 lists: offsets & max val
                self.offset_data[sus] = {dof: [[], []] for dof in ['P', 'Y']}

        # No fault dec here so we can control wrapup when exiting the state
        def run(self):
            def channel(bank, suffix='_OFFSET'):
                """This should be used only in run. It is a convenience function
                to easily get the channel. We ony use 3 channels variants in here:
                * SUS-ETMX_M0_TEST_P_OFFSET
                * SUS-ETMX_M0_TEST_P_TRAMP
                * SUS-ETMX_M0_OPTICALIGN_P_OFFSET
                suffix is defaulting to _OFFSET because
                that is used for the large majority of these calls. 
                Example use:

                >>> self.channel('TEST', 'P')
                 'SUS-ETMX_M0_TEST_P_OFFSET'
                """
                return self.gen_chan.format(sus=self.cur_sus,
                                            arm=arm,
                                            stage=self.levels[self.cur_sus],
                                            bank=bank,
                                            dof=self.cur_dof,
                                            suffix=suffix)
            self.channel = channel

            if self.flashes_high_enough:
                return True
            # FIXME: HACK! Used to wrap things up if we need to escape.
            elif ezca['GRD-ALS_{}ARM_REQUEST'.format(arm)] != 'INCREASE_FLASHES':
                notify('"Gracefully" exiting Inc Flashes')
                self.restore_offsets()
                return True
            # Wait here if we are in Fault, but should we just restore and then return FAULT?????
            elif is_fault(arm):
                notify('Arm in Fault state! Waiting...')
                log(ezca['ALS-'+arm+'_LOCK_ERROR_MSG'])
                log(ezca['ALS-'+arm+'_REFL_LOCK_ERROR_MSG'])
                log(ezca['ALS-'+arm+'_FIBR_LOCK_ERROR_MSG'])
                return False

            # Set offset, wait
            elif self.timer['offset_tramp'] and ezca[self.channel('TEST')] \
               != self.offset_list[self.cur_sus][self.cur_dof][self.offset_list_counter]:
                log('offset_list: {}'.format(self.offset_list))
                ezca[self.channel('TEST')] = self.offset_list[self.cur_sus][self.cur_dof][self.offset_list_counter]
                self.timer['offset_tramp'] = self.off_tramp
            # Offset is set, get data
            elif self.timer['offset_tramp'] and ezca[self.channel('TEST')] \
               == self.offset_list[self.cur_sus][self.cur_dof][self.offset_list_counter]:
                data = call_with_timeout(getdata, 'ALS-C_TR{}_A_LF_OUT_DQ'.format(arm), self.data_dur, seconds_timeout=self.data_dur+10)
                # Save our data
                self.offset_data[self.cur_sus][self.cur_dof][0].append(ezca[self.channel('TEST')])  #SED edit: this used to be a sum off the offset readback from the test offset, and the offset from the list (so it was twice the offset)  I changed it to just be the offset since I found this confusing
                self.offset_data[self.cur_sus][self.cur_dof][1].append(data.data.max())
                log('offset_data: {}'.format(self.offset_data))
                # Prep for next point
                if self.offset_list_counter < len(self.offset_list[self.cur_sus][self.cur_dof]) - 1:
                    self.offset_list_counter += 1
                ### We have all our data for all the points, where is the max in relation?
                else:
                    log('All the data, we have')
                    loc_max = max(self.offset_data[self.cur_sus][self.cur_dof][1])
                    log('loc_max: {}'.format(loc_max))
                    for i, maxi in enumerate(self.offset_data[self.cur_sus][self.cur_dof][1]):
                        if maxi == loc_max:
                            spot = self.offset_data[self.cur_sus][self.cur_dof][0][i]
                            log(spot)
                            break
                    # Our max is high enough, move there
                    if loc_max >= self.pwr_thresh[arm]:
                        log('We have a good spot!')
                        # Reset the TEST bank
                        # SED moved this offset to hardcoded in lscparams because if this code is exited before it finishes we will loose the offset
                        # Since we are above the thresh, reset the P & Y Test offsets
                        self.restore_offsets()
                        # FIXME: Remove after above is tested
                        #ezca['SUS-{}{}_M0_TEST_P_OFFSET'.format(self.cur_sus, arm)] = lscparams.misalign_offsets['ETM'][arm]['P']
                        #ezca['SUS-{}{}_M0_TEST_Y_OFFSET'.format(self.cur_sus, arm)] = lscparams.misalign_offsets['ETM'][arm]['Y']
                        #ezca.get_LIGOFilter('SUS-{}{}_M0_TEST_P'.format(self.cur_sus, arm, 'P')).switch_off('OFFSET')
                        #ezca.get_LIGOFilter('SUS-{}{}_M0_TEST_Y'.format(self.cur_sus, arm, 'Y')).switch_off('OFFSET')
                        # Place into OA bank only for this cur dof
                        ezca[self.channel('OPTICALIGN')] += spot
                        self.flashes_high_enough = True
                        return True
                        
                    # At the - end of our search range
                    if spot == min(self.offset_data[self.cur_sus][self.cur_dof][0]):
                        log('Its at the start of our offset list, getting more points')
                        log(self.offset_data)
                        # Set new offset
                        ezca[self.channel('TEST')] = spot
                        self.timer['offset_tramp'] = self.off_tramp
                        # Set new offset list, start from the current offset and go down -1.5 urad from there
                        new_range = [-1.5,-1, -0.5]
                        self.offset_list[self.cur_sus][self.cur_dof] = [x + spot for x in new_range] 
                        # Reset counter
                        self.offset_list_counter = 0
                        # It should do it all again now
                    # At the + end of our range
                    elif spot == max(self.offset_data[self.cur_sus][self.cur_dof][0]):
                        log('Its at the end of our offset list, getting more points')
                        # Set new offset
                        ezca[self.channel('TEST')] = spot
                        self.timer['offset_tramp'] = self.off_tramp
                        # Set new offset list
                        new_range = [0.5,1,1.5]
                        self.offset_list[self.cur_sus][self.cur_dof] = [x + spot for x in new_range]
                        # Reset counter
                        self.offset_list_counter = 0
                        # It should do it all again now

                    else:
                        log('Its in the middle of our range somewhere, but we are not above the threshold yet.')
                        # Reset the TEST bank and put the current changes into the OA bank
                        ezca[self.channel('TEST')] = lscparams.misalign_offsets['ETM'][arm][self.cur_dof]
                        ezca.get_LIGOFilter(self.channel('TEST', suffix='')).switch_off('OFFSET')
                        # Place into OA bank only for this cur dof
                        ezca[self.channel('OPTICALIGN')] += spot
                        # Order: ETM P, ETM Y, TMS P, TMS Y
                        if self.cur_dof == 'P':
                            log('Trying Y now')
                            self.cur_dof = 'Y'
                            self.offset_list_counter = 0
                            return False
                        elif self.cur_dof == 'Y' and self.cur_sus == self.sus[0]:
                            self.cur_sus = self.sus[1]
                            self.cur_dof = 'P'
                            log('Trying {} {} next'.format(self.cur_sus, self.cur_dof))
                            self.offset_list_counter = 0
                            return False
                        else:
                            notify('ETM and TMS pitch and yaw optimized, still below threshold')
                        # Reset the P & Y Test offsets
                        self.restore_offsets()
                        # FIXME: Remove after above is tested
                        #ezca['SUS-{}{}_M0_TEST_P_OFFSET'.format(self.cur_sus, arm)] = lscparams.misalign_offsets['ETM'][arm]['P']
                        #ezca['SUS-{}{}_M0_TEST_Y_OFFSET'.format(self.cur_sus, arm)] = lscparams.misalign_offsets['ETM'][arm]['Y']
                        #ezca.get_LIGOFilter('SUS-{}{}_M0_TEST_P'.format(self.cur_sus, arm, 'P')).switch_off('OFFSET')
                        #ezca.get_LIGOFilter('SUS-{}{}_M0_TEST_Y'.format(self.cur_sus, arm, 'Y')).switch_off('OFFSET')
                        # Set this just so we don't run through it again
                        self.flashes_high_enough = True
                        return True
      
    return INCREASE_FLASHES


def gen_LOCKING(arm):
    class LOCKING(GuardState):
        goto=True
        request = False

        @fault_checker_gen(arm)
        def main(self):
            if arm == 'X':
                #shutter end station green
                ezca['SYS-MOTION_X_SHUTTER_A_OPEN'] =1
            if arm == 'Y':
                ezca['SYS-MOTION_Y_SHUTTER_B_OPEN'] = 1
            # Request the lock
            ezca['ALS-'+self.arm+'_LOCK_ENABLE'] = 1
            ezca['ALS-C_LOCK_REQUEST'+self.arm] = 'End Locked'
            ezca['ALS-'+self.arm+'_VCO_CONTROLS_ENABLE'] = 0
            # WFS are turned off during locking, and history is dumped
            ezca['ALS-'+self.arm+'_WFS_SWITCH'] = 'OFF'
            for py in ['P', 'Y']:
                ezca['ALS-%s_WFS_DOF_1_%s_RSET'%(self.arm,py)] = 2
                ezca['ALS-%s_WFS_DOF_2_%s_RSET'%(self.arm,py)] = 2
                ezca['ALS-%s_WFS_DOF_3_%s_RSET'%(self.arm,py)] = 2

        @fault_checker_gen(arm)
        def run(self):
            if is_fault(self.arm):
                return 'FAULT'
            elif ezca['ALS-' + self.arm + '_REFL_LOCK_STATUS'] == 1:
            #SED added this elif Dec 17 2024 to check if the arm is locked before going on to enable WFS, because there was no check
                return True

    return LOCKING



# Im just putting this here for now, it will be moved later.

als_y_dict = {'H1:ALS-Y_CAM_ITM_PIT_ERR_OUT16': 0.04,
              'H1:ALS-Y_CAM_ITM_YAW_ERR_OUT16': 0.04,
              'H1:ALS-Y_WFS_A_I_PIT_OUT16': 40,
              'H1:ALS-Y_WFS_A_I_YAW_OUT16': 40,
              'H1:ALS-Y_WFS_B_I_PIT_OUT16': 150,
              'H1:ALS-Y_WFS_B_I_YAW_OUT16': 150,
              'H1:ALS-Y_WFS_DOF_1_P_OUT16': 70,
              'H1:ALS-Y_WFS_DOF_1_Y_OUT16': 70,
              'H1:ALS-Y_WFS_DOF_2_P_OUT16': 1000000.0,
              'H1:ALS-Y_WFS_DOF_2_Y_OUT16': 1000000.0,
              'H1:ALS-Y_WFS_DOF_3_P_OUT16': 100,
              'H1:ALS-Y_WFS_DOF_3_Y_OUT16': 100
}


als_x_dict = {'H1:ALS-X_CAM_ITM_PIT_ERR_OUT16': 0.04,
              'H1:ALS-X_CAM_ITM_YAW_ERR_OUT16': 0.04,
              'H1:ALS-X_WFS_A_I_PIT_OUT16': 40,
              'H1:ALS-X_WFS_A_I_YAW_OUT16': 40,
              'H1:ALS-X_WFS_B_I_PIT_OUT16': 150,
              'H1:ALS-X_WFS_B_I_YAW_OUT16': 150,
              'H1:ALS-X_WFS_DOF_1_P_OUT16': 80,
              'H1:ALS-X_WFS_DOF_1_Y_OUT16': 75,
              'H1:ALS-X_WFS_DOF_2_P_OUT16': 1000000.0,
              'H1:ALS-X_WFS_DOF_2_Y_OUT16': 1000000.0,
              'H1:ALS-X_WFS_DOF_3_P_OUT16': 100,
              'H1:ALS-X_WFS_DOF_3_Y_OUT16': 100
}

als_thresh_dict = {
    'X': als_x_dict,
    'Y': als_y_dict
}

def gen_INITIAL_ALIGNMENT(arm):
    class INITIAL_ALIGNMENT(GuardState):
        request = True

        @fault_checker_gen(arm)
        def main(self):
            ezca['SYS-MOTION_'+self.arm+'_SHUTTER_A_OPEN'] = 1 #Make sure the ALS shutter is open
            ezca['ALS-C_LOCK_REQUEST'+self.arm] = 'End Locked'

            #disable VCO controls
            ezca['ALS-'+self.arm+'_VCO_CONTROLS_ENABLE'] = 0
            self.timer['convergence_check'] = 30

        def run(self):
            if is_fault(self.arm):
                return 'FAULT'
            if ezca.read('ALS-'+self.arm+'_REFL_LOCK_STATE', as_string=True) != 'Locked':
                return 'LOCKING'
            if self.timer['convergence_check']:
                if avg_convergence_checker(als_thresh_dict[arm]):
                    # Put a timer here in case this is the requested state
                    self.timer['convergence_check'] = 10
                    return True
                # Give it a 5 second wait between going to grab more data
                self.timer['convergence_check'] = 5

    return INITIAL_ALIGNMENT


def gen_LOCKED_W_SLOW_FEEDBACK(arm):
    class LOCKED_W_SLOW_FEEDBACK(GuardState):
        request = False

        
        def main(self):
            ezca['ALS-C_LOCK_REQUEST'+self.arm]= 'Slow Engaged'
            self.timer['pause'] = 1

            self.dof3gain = lscparams.gain['ALS_ASC_DOF3'][arm]

            self.lowASCgain = False # Initialize
            if (   (ezca['ALS-'+self.arm+'_WFS_DOF_3_P_GAIN'] != self.dof3gain)
                or (ezca['ALS-'+self.arm+'_WFS_DOF_3_Y_GAIN'] != self.dof3gain)):
                log('Camera centering ASC gain was low.')
                self.lowASCgain = True

        def run(self):
            if is_fault(self.arm):
                return 'FAULT'
            if ezca.read('ALS-'+self.arm+'_REFL_LOCK_STATE', as_string=True) != 'Locked':
                return 'LOCKING'
            elif self.timer['pause']:
                if self.lowASCgain:
                    if (    (abs(ezca['ALS-'+arm+'_CAM_ITM_PIT_DIFF']) < 15)
                        and (abs(ezca['ALS-'+arm+'_CAM_ITM_YAW_DIFF']) < 15)):
                        log('Camera centering ASC gain reset to nominal')
                        ezca['ALS-'+self.arm+'_WFS_DOF_3_P_GAIN'] = self.dof3gain
                        ezca['ALS-'+self.arm+'_WFS_DOF_3_Y_GAIN'] = self.dof3gain
                        self.lowASCgain = False
                else:
                    return True

    return LOCKED_W_SLOW_FEEDBACK


def gen_ENABLE_WFS_PUM(arm):
    class ENABLE_WFS(GuardState):
        request = False

        @fault_checker_gen(arm)
        def main(self):
            self.arm = arm # Jnote: why do I need to do this to make it work? JCD 11June2018

            self.dof_gain={\
                    'P1':-1000*0.5,\
                    'P2':-2500*0.5,\
                    'P3':2.5,\
                    'Y1':-500,\
                    'Y2':5000*0.5,\
                    'Y3':1\
                    }
            #SED reduced March 16 2022 since DOF1 Y was going unstable for Y arm. 
            for py in ['P', 'Y']:

                ### prep ctrl
                for iDof in range(1,4):
                    ezca['ALS-%s_WFS_DOF_%i_%s_GAIN'%(self.arm,iDof,py)]=0
                    ezca['ALS-%s_WFS_DOF_%i_%s_TRAMP'%(self.arm,iDof,py)]=20

                    ezca.get_LIGOFilter('ALS-%s_WFS_DOF_%i_%s'%(self.arm,iDof, py)).switch_off\
                        ('FM2', 'FM3', 'FM4', 'FM5', 'FM6',\
                         'FM7', 'FM8', 'INPUT')
                    ezca.get_LIGOFilter('ALS-%s_WFS_DOF_%i_%s'%(self.arm,iDof, py)).switch_on\
                        ('FM9', 'FM10', 'OUTPUT', 'DECIMATION')

                # HY 08/17/18 move the LP0.3 from WFS filter bank to the ctrl filter bank
                ezca.get_LIGOFilter('ALS-%s_WFS_DOF_2_%s'%(self.arm, py)).switch_on('FM8')

                ### prep sus
                ezca['SUS-ETM%s_L2_LOCK_OUTSW_%s'%(self.arm,py)] = 1 # Turn on PUM
                ezca['SUS-ITM%s_L2_LOCK_OUTSW_%s'%(self.arm,py)] = 1

                if py=='P':
                    ezca.get_LIGOFilter('SUS-ETM%s_M0_LOCK_%s'%(self.arm,py)).only_on('FM3','FM4','FM5', 'FM7', 'FM8', 'OUTPUT','DECIMATION')
                    ezca.get_LIGOFilter('SUS-ITM%s_M0_LOCK_%s'%(self.arm,py)).only_on('FM3','FM4','FM5', 'FM7', 'FM8', 'OUTPUT','DECIMATION')

                else:
                    ezca.get_LIGOFilter('SUS-ETM%s_M0_LOCK_%s'%(self.arm,py)).only_on('FM3','FM4','FM5', 'FM8', 'OUTPUT','DECIMATION')
                    ezca.get_LIGOFilter('SUS-ITM%s_M0_LOCK_%s'%(self.arm,py)).only_on('FM3','FM4','FM5', 'FM8', 'OUTPUT','DECIMATION')

                ezca.get_LIGOFilter('SUS-TMS%s_M1_LOCK_%s'%(self.arm,py)).only_on('FM1','FM2','FM3', 'FM10', 'OUTPUT','DECIMATION')

                ezca['SUS-ETM%s_L2_LOCK_OUTSW_%s'%(self.arm,py)] = 1
                ezca['SUS-ITM%s_L2_LOCK_OUTSW_%s'%(self.arm,py)] = 1

                ezca.switch('ALS-%s_WFS_DOF_1_%s'%(self.arm,py),'INPUT','ON')
                ezca.switch('ALS-%s_WFS_DOF_2_%s'%(self.arm,py),'INPUT','ON')
                ezca.switch('ALS-%s_WFS_DOF_3_%s'%(self.arm,py),'INPUT','ON')

                #for iDof in range(3):
                #    ezca['ALS-%s_WFS_DOF_%i_%s_GAIN'%(self.arm,iDof+1,py)]=self.dof_gain['%s%i'%(py, iDof+1)]

            ### FIXME ###

            # If camera centering is too far off, turn the loop on with lower gain.  Gain to nominal value in Locked_w_slow_feedback state
            #SED comment, is this really needed?  It seems that for the X arm this hasn't been actually turned up
            if ((abs(ezca['ALS-'+arm+'_CAM_ITM_PIT_DIFF']) > 15) or (abs(ezca['ALS-'+arm+'_CAM_ITM_YAW_DIFF']) > 15)):
                log('Camera centering too far off.  Need to turn on slowly.')
                #SED is commenting this out since they nver get set to nominal, and make X arm WFS extremly slow.  If it is needed, when commenting it back in make sure that latter the gains get set to nominal
                #self.dof_gain['P3'] = self.dof_gain['P3']*0.3
                #self.dof_gain['Y3'] = self.dof_gain['Y3']*0.3
                self.flag3=False
            else:
                self.flag3=True
            #else:
            #    self.dof3gain = lscparams.gain['ALS_ASC_DOF3'][arm]
            #ezca['ALS-'+self.arm+'_WFS_DOF_3_P_GAIN'] = self.dof3gain
            #ezca['ALS-'+self.arm+'_WFS_DOF_3_Y_GAIN'] = self.dof3gain

            ezca['ALS-'+self.arm+'_WFS_SWITCH'] = 'ON'
            ezca['ALS-'+self.arm+'_WFS_DOF_MASK_FM1'] = 1 # Double check that the triggering is on
            self.timer['WFSconverge'] = 10 # initialize
            #self.timer['WFSpause'] = 30
            self.flag1=False
            self.flag2=False

            ### FIXME ###
            ### hard coded
            self.dofList=[1, 2]
            if self.arm.upper()=='X':
                self.PitTol=[500, 3.e5]
                self.YawTol=[1750, 7.5e5]
            else:
                self.PitTol=[300, 5.e5]
                self.YawTol=[200, 3.e5]

        @fault_checker_gen(arm)
        def run(self):
            if is_fault(self.arm):
                return 'FAULT'
            if ezca.read('ALS-'+self.arm+'_REFL_LOCK_STATE', as_string=True) != 'Locked':
                return 'LOCKING'

            if not (ezca['ALS-%s_WFS_DOF_FM_TRIG_DELAYED'%self.arm] or ezca['ALS-%s_WFS_TRIG_DELAYED'%self.arm]):
                self.timer['WFSconverge'] = 5 # wait 5 secs after trigs come on
                #self.timer['WFSpause'] = 60
            else:
                if self.timer['WFSconverge'] and not self.flag1:
                    for py in ['P', 'Y']:
                        ezca.switch('SUS-ETM%s_M0_LOCK_%s'%(self.arm,py),'INPUT','ON')
                        ezca.switch('SUS-ITM%s_M0_LOCK_%s'%(self.arm,py),'INPUT','ON')
                        ezca.switch('SUS-TMS%s_M1_LOCK_%s'%(self.arm,py),'INPUT','ON')
                        ezca['SUS-TMS%s_M1_LOCK_%s_GAIN'%(self.arm,py)] = 1

                        for iDof in range(3):
                            ezca['ALS-%s_WFS_DOF_%i_%s_GAIN'%(self.arm,iDof+1,py)]=self.dof_gain['%s%i'%(py, iDof+1)]

                    ezca['SUS-ETM%s_M0_LOCK_P_GAIN'%(self.arm)] = 0.3
                    ezca['SUS-ITM%s_M0_LOCK_P_GAIN'%(self.arm)] = 0.3
                    ezca['SUS-ETM%s_M0_LOCK_Y_GAIN'%(self.arm)] = 1
                    ezca['SUS-ITM%s_M0_LOCK_Y_GAIN'%(self.arm)] = 1
                    self.flag1=True

                #if self.timer['WFSpause'] and not self.flag2:
                if ISC_library.alsWfs_convergence_checker(self.dofList, self.PitTol, self.YawTol, self.arm) and not self.flag2:
                    for py in ['P', 'Y']:
                        for iDof in range(3):
                            ezca.get_LIGOFilter('ALS-%s_WFS_DOF_%i_%s'%(self.arm,iDof+1, py)).switch_off('FM9')
                    self.flag2=True

                if ISC_library.alsWfs_convergence_checker([3], [50], [50], self.arm) and not self.flag3:
                    for py in ['P', 'Y']:
                        ezca['ALS-%s_WFS_DOF_3_%s_GAIN'%(self.arm,py)]=self.dof_gain['%s3'%(py)]
                    self.flag3=True

                if self.flag1 and self.flag2 and self.flag3:
                    return True

    return ENABLE_WFS


def gen_GREEN_WFS_OFFLOADED(arm):
    class GREEN_WFS_OFFLOADED(GuardState):
        request = True
        def main(self):
            return True
    return GREEN_WFS_OFFLOADED


def gen_LOCKED_SLOW_NO_WFS(arm):
    class LOCKED_SLOW_NO_WFS(GuardState):
        request = True

        @fault_checker_gen(arm)
        def main(self):
            self.arm=arm
            for py in ['P','Y']:
                ezca.switch('SUS-ETM%s_M0_LOCK_%s'%(self.arm,py), 'INPUT', 'OFF')
                ezca.switch('SUS-ITM%s_M0_LOCK_%s'%(self.arm,py), 'INPUT', 'OFF')
                ezca.switch('SUS-TMS%s_M1_LOCK_%s'%(self.arm,py), 'INPUT', 'OFF')
            ezca['ALS-'+self.arm+'_WFS_SWITCH'] = 'OFF'
            ezca['ALS-C_LOCK_REQUEST'+self.arm] = 'Slow Engaged'

        @fault_checker_gen(arm)
        def run(self):
            if is_fault(self.arm):
                return 'FAULT'
            if ezca.read('ALS-'+self.arm+'_REFL_LOCK_STATE', as_string=True) != 'Locked':
                return 'LOCKING'
            else:
                return True

    return LOCKED_SLOW_NO_WFS


def gen_LOCKED_SLOW_W_GR_WFS_PUM(arm):
    class LOCKED_SLOW_W_GR_WFS_PUM(GuardState):
        request = False

        @fault_checker_gen(arm)
        def main(self):
            self.arm=arm
            self.dof_gain={\
                    'P1':-300,\
                    'P2':-800,\
                    'P3':5,\
                    'Y1':-500,\
                    'Y2':1500,\
                    'Y3':2\
                    }
            ezca['ALS-'+self.arm+'_WFS_SWITCH'] = 'ON'
            ezca['ALS-C_LOCK_REQUEST'+self.arm]= 'Slow Engaged'

            for py in ['P', 'Y']:
                ezca['ALS-%s_WFS_DOF_1_%s_GAIN'%(self.arm, py)]=0
                ezca['ALS-%s_WFS_DOF_2_%s_GAIN'%(self.arm, py)]=0
                ezca['ALS-%s_WFS_DOF_1_%s_TRAMP'%(self.arm, py)]=10
                ezca['ALS-%s_WFS_DOF_2_%s_TRAMP'%(self.arm, py)]=10

                ezca['SUS-ETM%s_L2_LOCK_OUTSW_%s'%(self.arm,py)] = 1 # Turn on PUM
                if py=='P':
                    ezca.get_LIGOFilter('SUS-ETM%s_M0_LOCK_%s'%(self.arm,py)).only_on('FM3', 'FM4', 'FM5', 'FM7', 'FM8', 'OUTPUT', 'DECIMATION')
                    ezca.get_LIGOFilter('SUS-ITM%s_M0_LOCK_%s'%(self.arm,py)).only_on('FM3', 'FM4', 'FM5', 'FM7', 'FM8', 'OUTPUT', 'DECIMATION')
                else:
                    ezca.get_LIGOFilter('SUS-ETM%s_M0_LOCK_%s'%(self.arm,py)).only_on('FM3', 'FM4', 'FM5', 'FM8', 'OUTPUT', 'DECIMATION')
                    ezca.get_LIGOFilter('SUS-ITM%s_M0_LOCK_%s'%(self.arm,py)).only_on('FM3', 'FM4', 'FM5', 'FM8', 'OUTPUT', 'DECIMATION')
                ezca['SUS-ETM%s_L2_LOCK_OUTSW_%s'%(self.arm,py)] = 1

                ezca.get_LIGOFilter('SUS-TMS%s_M1_LOCK_%s'%(self.arm,py)).only_on('FM1','FM2','FM3', 'FM10', 'OUTPUT','DECIMATION')

                ezca.get_LIGOFilter('ALS-%s_WFS_DOF_1_%s'%(self.arm,py)).switch_off('FM2', 'FM3', 'FM4', 'FM5', 'FM6',\
                                                                                     'FM7', 'FM8')
                ezca.get_LIGOFilter('ALS-%s_WFS_DOF_1_%s'%(self.arm,py)).switch_on('FM9', 'FM10', 'OUTPUT', 'DECIMATION')

                ezca.get_LIGOFilter('ALS-%s_WFS_DOF_2_%s'%(self.arm,py)).switch_off('FM2', 'FM3', 'FM4', 'FM5', 'FM6', 'FM7')
                ezca.get_LIGOFilter('ALS-%s_WFS_DOF_2_%s'%(self.arm,py)).switch_on('FM8', 'FM9', 'FM10', 'OUTPUT', 'DECIMATION')
                # HY 08/17/18 moved the LP0.3 in the WFS filter bank to ctrl filter bank FM8



            self.timer['WFS converge'] = 10
            self.timer['WFSpause'] = 13
            self.flag1=False
            self.flag2=False

            ### FIXME ###
            ### hard coded
            self.dofList=[1, 2]
            if self.arm.upper()=='X':
                self.PitTol=[500, 3.e5]
                self.YawTol=[1750, 7.5e5]
            else:
                self.PitTol=[300, 5.e5]
                self.YawTol=[200, 3.e5]

        @fault_checker_gen(arm)
        def run(self):
            if is_fault(self.arm):
                return 'FAULT'
            if ezca.read('ALS-'+self.arm+'_REFL_LOCK_STATE', as_string=True) != 'Locked':
                return 'LOCKING'
            else:
                if self.timer['WFS converge'] and not self.flag1:
                    if ezca['ALS-C_TR%s_A_LF_OUT16'%self.arm] < 0.5: #if we are locked to a higher-order mode
                        return 'UNLOCKED'
                    for py in ['P', 'Y']:
                        ezca.get_LIGOFilter('ALS-%s_WFS_DOF_1_%s'%(self.arm,py)).switch_on('INPUT')
                        #ezca.get_LIGOFilter('ALS-%s_WFS_DOF_2_%s'%(self.arm,py)).switch_on('INPUT')
                        ezca.get_LIGOFilter('ALS-%s_WFS_DOF_3_%s'%(self.arm,py)).switch_off('INPUT')

                        ezca['ALS-%s_WFS_DOF_1_%s_GAIN'%(self.arm, py)]=self.dof_gain['%s1'%py]
                        #ezca['ALS-%s_WFS_DOF_2_%s_GAIN'%(self.arm, py)]=self.dof_gain['%s2'%py]
                        ezca.switch('SUS-ETM%s_M0_LOCK_%s'%(self.arm,py),'INPUT','ON')
                        #ezca.switch('SUS-TMS%s_M1_LOCK_%s'%(self.arm,py),'INPUT','ON')
                        ezca['SUS-ETM%s_M0_LOCK_%s_GAIN'%(self.arm,py)] = 1
                        #ezca['SUS-TMS%s_M1_LOCK_%s_GAIN'%(self.arm,py)] = 1
                    self.flag1=True

                if self.timer['WFSpause'] and not self.flag2:
                    if ISC_library.alsWfs_convergence_checker(self.dofList, self.PitTol, self.YawTol, self.arm) and not self.flag2:
                        for py in ['P', 'Y']:
                            ezca.get_LIGOFilter('ALS-%s_WFS_DOF_1_%s'%(self.arm,py)).switch_off('FM9')
                            #ezca.get_LIGOFilter('ALS-%s_WFS_DOF_2_%s'%(self.arm,py)).switch_off('FM9')
                        self.flag2=True

                if self.flag1 and self.flag2:
                    return True

    return LOCKED_SLOW_W_GR_WFS_PUM

def gen_LOCKED_SLOW_WFS_ETM_TMS(arm):
    class LOCKED_SLOW_WFS_ETM_TMS(GuardState):
        request = True

        @fault_checker_gen(arm)
        def main(self):
            self.arm=arm
            self.dof_gain = {
                'P1':-300,
                'P2':-1000,
                'P3':5,
                'Y1':-500,
                'Y2':500,
                'Y3':2
            }
            ezca['ALS-'+self.arm+'_WFS_SWITCH'] = 'ON'
            ezca['ALS-C_LOCK_REQUEST'+self.arm]= 'Slow Engaged'

            for py in ['P', 'Y']:
                ezca['ALS-%s_WFS_DOF_1_%s_GAIN'%(self.arm, py)]=0
                ezca['ALS-%s_WFS_DOF_2_%s_GAIN'%(self.arm, py)]=0
                ezca['ALS-%s_WFS_DOF_1_%s_TRAMP'%(self.arm, py)]=10
                ezca['ALS-%s_WFS_DOF_2_%s_TRAMP'%(self.arm, py)]=10

                ezca['SUS-ETM%s_L2_LOCK_OUTSW_%s'%(self.arm,py)] = 1 # Turn on PUM
                if py=='P':
                    ezca.get_LIGOFilter('SUS-ETM%s_M0_LOCK_%s'%(self.arm,py)).only_on('FM3', 'FM4', 'FM5', 'FM7', 'FM8', 'OUTPUT', 'DECIMATION')
                    ezca.get_LIGOFilter('SUS-ITM%s_M0_LOCK_%s'%(self.arm,py)).only_on('FM3', 'FM4', 'FM5', 'FM7', 'FM8', 'OUTPUT', 'DECIMATION')
                else:
                    ezca.get_LIGOFilter('SUS-ETM%s_M0_LOCK_%s'%(self.arm,py)).only_on('FM3', 'FM4', 'FM5', 'FM8', 'OUTPUT', 'DECIMATION')
                    ezca.get_LIGOFilter('SUS-ITM%s_M0_LOCK_%s'%(self.arm,py)).only_on('FM3', 'FM4', 'FM5', 'FM8', 'OUTPUT', 'DECIMATION')
                ezca['SUS-ETM%s_L2_LOCK_OUTSW_%s'%(self.arm,py)] = 1

                ezca.get_LIGOFilter('SUS-TMS%s_M1_LOCK_%s'%(self.arm,py)).only_on('FM1','FM2','FM3', 'FM10', 'OUTPUT','DECIMATION')

                ezca.get_LIGOFilter('ALS-%s_WFS_DOF_1_%s'%(self.arm,py)).switch_off('FM2', 'FM3', 'FM4', 'FM5', 'FM6',\
                                                                                     'FM7', 'FM8')
                ezca.get_LIGOFilter('ALS-%s_WFS_DOF_1_%s'%(self.arm,py)).switch_on('FM9', 'FM10', 'OUTPUT', 'DECIMATION')
                
                ezca.get_LIGOFilter('ALS-%s_WFS_DOF_2_%s'%(self.arm,py)).switch_off('FM2', 'FM3', 'FM4', 'FM5', 'FM6', 'FM7')
                ezca.get_LIGOFilter('ALS-%s_WFS_DOF_2_%s'%(self.arm,py)).switch_on('FM8', 'FM9', 'FM10', 'OUTPUT', 'DECIMATION')



            self.timer['WFS converge'] = 10
            self.timer['WFSpause'] = 13
            self.counter = 0
            self.flag2=False

            ### FIXME ###
            ### hard coded
            self.dofList=[1, 2]
            if self.arm.upper()=='X':
                self.PitTol=[500, 3.e6]
                self.YawTol=[1750, 3e6]
            else:
                self.PitTol=[300, 1e6]
                self.YawTol=[300, 1e6]

        @fault_checker_gen(arm)
        def run(self):
            if is_fault(self.arm):
                return 'FAULT'
            if ezca.read('ALS-'+self.arm+'_REFL_LOCK_STATE', as_string=True) != 'Locked':
                return 'LOCKING'
            else:
                if self.timer['WFS converge'] and self.counter == 0:
                    if ezca['ALS-C_TR%s_A_LF_OUT16'%self.arm] < 0.5: #if we are locked to a higher-order mode
                        return 'UNLOCKED'
                    for py in ['P', 'Y']:
                        ezca.get_LIGOFilter('ALS-%s_WFS_DOF_1_%s'%(self.arm,py)).switch_on('INPUT')

                        # CRC August 24, 2022
                        # Only turn on the X-arm DOF 2 loops, because Y-arm DOF 2 loops are causing locklosses
                        # if self.arm == "X":
                        ezca.get_LIGOFilter('ALS-%s_WFS_DOF_2_%s'%(self.arm,py)).switch_on('INPUT')
                        # else:
                        #    ezca.get_LIGOFilter('ALS-%s_WFS_DOF_2_%s'%(self.arm,py)).switch_off('INPUT')

                        ezca.get_LIGOFilter('ALS-%s_WFS_DOF_3_%s'%(self.arm,py)).switch_off('INPUT')

                        ezca['ALS-%s_WFS_DOF_1_%s_GAIN'%(self.arm, py)]=self.dof_gain['%s1'%py]
                        ezca['ALS-%s_WFS_DOF_2_%s_GAIN'%(self.arm, py)]=self.dof_gain['%s2'%py]
                        ezca.switch('SUS-ETM%s_M0_LOCK_%s'%(self.arm,py),'INPUT','ON')
                        ezca.switch('SUS-TMS%s_M1_LOCK_%s'%(self.arm,py),'INPUT','ON')
                        ezca['SUS-ETM%s_M0_LOCK_%s_GAIN'%(self.arm,py)] = 1
                        ezca['SUS-TMS%s_M1_LOCK_%s_GAIN'%(self.arm,py)] = 1
                    self.counter += 1

                if self.counter == 1 and ezca['ALS-%s_WFS_TRIG_DELAYED'%(self.arm)]:
                    self.timer['WFSpause'] = 13
                    self.counter += 1
                #once the WFS are on, there is a delayed boost which comes on.  We need to let the boost come on then wait before we check for convergence or offload
                if self.timer['WFSpause'] and self.counter == 2:
                    if ISC_library.alsWfs_convergence_checker(self.dofList, self.PitTol, self.YawTol, self.arm) and not self.flag2:
                        for py in ['P', 'Y']:
                            ezca.get_LIGOFilter('ALS-%s_WFS_DOF_1_%s'%(self.arm,py)).switch_off('FM9')
                            ezca.get_LIGOFilter('ALS-%s_WFS_DOF_2_%s'%(self.arm,py)).switch_off('FM9')
                        self.counter += 1

                if self.counter ==3:
                    if ISC_library.alsWfs_convergence_checker(self.dofList, self.PitTol, self.YawTol, self.arm):
                        log('returning true')
                        return True
                    #return True
    return LOCKED_SLOW_WFS_ETM_TMS

def gen_ETM_TMS_WFS_OFFLOADED(arm):
    class ETM_TMS_WFS_OFFLOADED(GuardState):
        request = True
        @fault_checker_gen(arm)
        def run(self):
            if ezca.read('ALS-'+self.arm+'_REFL_LOCK_STATE', as_string=True) != 'Locked':
                return 'LOCKING'
            else:
                return True
    return ETM_TMS_WFS_OFFLOADED

def gen_NO_SLOW_W_GR_WFS(arm):
    class NO_SLOW_W_GR_WFS(GuardState):
        request = False

        @fault_checker_gen(arm)
        def main(self):
            self.arm=arm
            #for i in range(2):
            #    for py in ['P', 'Y']:
            #        dof_gain=ezca['ALS-%s_WFS_DOF_1_%s_GAIN'%(self.arm, py)]
            #        ezca['ALS-%s_WFS_DOF_1_%s_GAIN'%(self.arm, py)]=dof_gain*0.1

            ezca['ALS-C_LOCK_REQUEST'+self.arm]= 'End Locked'

        @fault_checker_gen(arm)
        def run(self):
            if is_fault(self.arm):
                return 'FAULT'
            if ezca.read('ALS-'+self.arm+'_REFL_LOCK_STATE', as_string=True) != 'Locked':
                return 'LOCKING'
            else:
                return True

    return NO_SLOW_W_GR_WFS

def gen_TRANSITION(arm):
    class TRANSITION(GuardState):
        request = False

        @fault_checker_gen(arm)
        def main(self):
            ezca['ALS-C_LOCK_REQUEST'+arm] = 'Transition'
            self.timer['pause'] = 0.1

        @fault_checker_gen(arm)
        def run(self):
            if self.timer['pause']:
                return True

    return TRANSITION

def gen_LOCKED_RED(arm):
    class LOCKED_RED(GuardState):
        request = False

        #@fault_checker_gen(arm)
        def main(self):
            ezca['ALS-'+self.arm+'_WFS_SWITCH'] = 'OFF'
            ezca['ALS-C_LOCK_REQUEST'+self.arm]= 'Red Locked'
            for py in ['P','Y']:
                for dofnum in range(1,4):
                    ezca.get_LIGOFilter('ALS-%s_WFS_DOF_%s_%s'%(self.arm, str(dofnum), py)).ramp_gain(0, 0.2, wait=False)

        #@fault_checker_gen(arm)
        def run(self):
            #if is_fault(self.arm):
            #    return 'FAULT'
            #if ezca.read('ALS-'+self.arm+'_REFL_LOCK_STATE', as_string=True) != 'Locked':
            #    return 'LOCKING'
            return True

    return LOCKED_RED


def gen_SHUTTERED():
    class SHUTTERED(GuardState):
        index = 100
        goto = False

        def run(self):
            return True

    return SHUTTERED
    
WAIT = 120 #seconds




my_nested_dict={ #initialize dictionary
    'X':{
        'x':{'pad':'SYS-FIBER_POL_CORR_X1_POSITION_REQ_DEG',
            'neg_pos':12,
            'pos_pos':13},
        'y':{'pad':'SYS-FIBER_POL_CORR_Y1_POSITION_REQ_DEG',
            'neg_pos':14,
            'pos_pos':15},
        'z':{'pad':'SYS-FIBER_POL_CORR_Z1_POSITION_REQ_DEG',
            'neg_pos':16,
            'pos_pos':17},
        },
    'Y':{
        'x':{'pad':'SYS-FIBER_POL_CORR_X2_POSITION_REQ_DEG',
            'neg_pos':18,
            'pos_pos':19},
        'y':{'pad':'SYS-FIBER_POL_CORR_Y2_POSITION_REQ_DEG',
            'neg_pos':20,
            'pos_pos':21},
        'z':{'pad':'SYS-FIBER_POL_CORR_Z2_POSITION_REQ_DEG',
            'neg_pos':22,
            'pos_pos':23}
        }
    }
    
def pol_command(arm, dof, posneg): #create a function to make steps

    if ezca['SYS-FIBER_POL_CORR_ERROR_FLAG'] == 1: #if there is an error while the code is running, the code will exit
        #restore button
        ezca['SYS-FIBER_POL_CORR_COMMAND_REQ'] = 24
        ezca['SYS-FIBER_POL_CORR_COMMAND_REQ'] = 0
        log('system error! breaking out of code.')
        return 'FAULT' #??

    ezca['SYS-FIBER_POL_CORR_COMMAND_REQ'] = my_nested_dict[arm][dof][posneg]
    time.sleep(0.1)

    while ezca['SYS-FIBER_POL_CORR_BUSY'] == 1:
        time.sleep(0.1)

    ezca['SYS-FIBER_POL_CORR_COMMAND_REQ'] = 0
    time.sleep(0.1)

    while ezca['SYS-FIBER_POL_CORR_BUSY'] == 1:
        time.sleep(0.1)
        
    time.sleep(0.5) #make sure the step is read after the adjustment
 
def gen_CHANGE_POL(arm):

    class CHANGE_POL(GuardState):

        #index = 500
        request = False

        def main(self):

            if ezca['CDS-PULIZZI_ACPWRCTRL_MSR0_OUTLET_1_ON_CMD'] == 0:
                ezca['CDS-PULIZZI_ACPWRCTRL_MSR0_OUTLET_1_ON_CMD'] = 1

            log('waiting for controller to settle')
            time.sleep(10)

            #restore button
            ezca['SYS-FIBER_POL_CORR_COMMAND_REQ'] = 24
            time.sleep(.05)
            ezca['SYS-FIBER_POL_CORR_COMMAND_REQ'] = 0
            time.sleep(10)

            #self.timer['Fine tuning polarization'] = WAIT

            self.complete = False
            self.counter = 0

        def run(self):

            if self.complete == False and self.counter <= 1:

                for dof, values in my_nested_dict[arm].items():
                
                    if ezca['SYS-FIBER_POL_CORR_ERROR_FLAG'] == 1:
                        log('controller in error. turning off controller')
                        ezca['CDS-PULIZZI_ACPWRCTRL_MSR0_OUTLET_1_OFF_CMD'] = 1
                        return 'FAULT'
						
                    if ezca['SYS-FIBER_POL_CORR_ERROR_FLAG'] == 0 and ezca['SYS-FIBER_POL_CORR_BUSY'] == 0: #check if there is an error/system is busy
                        if cdsutils.avg(5, ('ALS-{}_FIBR_LOCK_FIBER_POLARIZATIONPERCENT'.format(arm))) < 0:
                            log('Polarization percent is below 0. Jumping to fault!')
                            return 'FAULT'

                        for pn in ['pos_pos','neg_pos']:

                            als_var = ezca['ALS-{}_FIBR_LOCK_FIBER_POLARIZATIONPERCENT'.format(arm)]
                            log(als_var)
                            ezca['SYS-FIBER_POL_CORR_STEP_SIZE'] = 10.00

                            pol_command(arm,dof,pn)

                            als_var2 = ezca['ALS-{}_FIBR_LOCK_FIBER_POLARIZATIONPERCENT'.format(arm)]
                            log('The first value is {} the second value is {}'.format(als_var,als_var2))

                            if round(als_var2) > round(als_var): #added rounds here to make sure that the step actually got worse
                                if pn == 'pos_pos':
                                    pn = 'neg_pos'
                                    pol_command(arm,dof,pn)
                                else:
                                    pn = 'pos_pos'
                                    pol_command(arm,dof,pn)

                            else:

                                ezca['SYS-FIBER_POL_CORR_STEP_SIZE'] = 5.00 #lower steps values for more fine tuning
                                pol_command(arm,dof,pn)
                                als_var3 = ezca['ALS-{}_FIBR_LOCK_FIBER_POLARIZATIONPERCENT'.format(arm)]
                                log('var3 value is {}'.format(als_var3))

                                if round(als_var3,1) > round(als_var2,1):
                                    log('Went too far, moving on.')

                                    if pn == 'pos_pos':
                                        pn = 'neg_pos'
                                        pol_command(arm,dof,pn)
                                    else:
                                        pn = 'pos_pos'
                                        pol_command(arm,dof,pn)

                                    continue

                                while als_var2 >= als_var3: #self.timer['Fine tuning polarization'] and: #final loop tuning with lower step size

                                    ezca['SYS-FIBER_POL_CORR_STEP_SIZE'] = 4.00

                                    pol_command(arm,dof,pn)
                                    als_var4 = ezca['ALS-{}_FIBR_LOCK_FIBER_POLARIZATIONPERCENT'.format(arm)]
                                    log('var4 value is {}'.format(als_var4))

                                    if round(als_var3,3) < round(als_var4,3):
                                        log('var3 final value is {}'.format(als_var3))

                                        if pn == 'pos_pos':
                                            pn = 'neg_pos'
                                            pol_command(arm,dof,pn)
                                        else:
                                            pn = 'pos_pos'
                                            pol_command(arm,dof,pn)

                                        break

                                    pol_command(arm,dof,pn)
                                    als_var5 = ezca['ALS-{}_FIBR_LOCK_FIBER_POLARIZATIONPERCENT'.format(arm)]
                                    log('var5 value is {}'.format(als_var5))

                                    if round(als_var5,3) > round(als_var4,3):
                                        log('final value is {}'.format(als_var4))

                                        if pn == 'pos_pos':
                                            pn = 'neg_pos'
                                            pol_command(arm,dof,pn)
                                        else:
                                            pn = 'pos_pos'
                                            pol_command(arm,dof,pn)

                                        break
            else:
                if ezca['CDS-PULIZZI_ACPWRCTRL_MSR0_OUTLET_1_STATUS'] != 2:
                    ezca['CDS-PULIZZI_ACPWRCTRL_MSR0_OUTLET_1_OFF_CMD'] = 1
                return True

            if ezca['ALS-{}_FIBR_LOCK_FIBER_POLARIZATIONPERCENT'.format(arm)] < 20:
                self.complete = True
            elif ezca['ALS-{}_FIBR_LOCK_FIBER_POLARIZATIONPERCENT'.format(arm)] >= 20 \
                    and self.counter < 1:
                self.counter += 1
                self.complete = False
                log('Completed one try, will try again')
                return False
            else:
                log('Tried 2x, couldnt reduce polarization below {}'.format(ezca['ALS-{}_FIBR_LOCK_FIBER_POLARIZATIONPERCENT'.format(arm)]))
                return 'FAULT'



    return CHANGE_POL


# From LLO
##################################################
def spiral_waveforms(a, f1, f2, f_sample, t_duration):
    t_sample = 1.0/f_sample
    t = np.arange(0, t_duration, t_sample)
    r = (2*a/np.pi)*np.arcsin(np.sin(2*np.pi*f2*t))
    theta = 2*np.pi*f1*t
    sgn = np.sign(r)
    X = sgn*r*np.sin(theta)
    Y = r*np.cos(theta)
    return X,Y

def offload_test_alignment(ARM, sus, dof, test_output, tramp=3.0):
    if sus == 'TMS':
        stage = 'M1'
    else:
        stage = 'M0'
    oagain = ezca[':SUS-{}{}_{}_OPTICALIGN_{}_GAIN'.format(sus,ARM,stage,dof)]
    #test_output = ezca[':SUS-{}{}_{}_TEST_{}_OUTPUT'.format(sus,ARM,stage,dof)]
    offset = test_output/oagain
    ezca[':SUS-{}{}_{}_OPTICALIGN_{}_TRAMP'.format(sus,ARM,stage,dof)] = tramp
    time.sleep(0.1)
    ezca[':SUS-{}{}_{}_OPTICALIGN_{}_OFFSET'.format(sus,ARM,stage,dof)] += offset
    ezca.switch(':SUS-{}{}_{}_TEST_{}'.format(sus,ARM,stage,dof),'HOLD','OFF')

def pdh_ctrl(ARM, cmd, boost=True):
    """ON or OFF for PDH servo"""
    if cmd == 'ON':
        v = 1
    elif cmd == 'OFF':
        v = 0
    else:
        raise ValueError()
    ezca[f'ALS-{ARM}_REFL_SERVO_IN1EN'] = v
    time.sleep(0.6)
    ezca[f'ALS-{ARM}_REFL_SERVO_COMCOMP'] = v
    time.sleep(0.6)
    if boost:
        ezca[f'ALS-{ARM}_REFL_SERVO_COMBOOST'] = v


# TODO: 
"""
Make a way to escape this cleanly (In, but testing needed)
decorators (In but teseting needed)
where to jump (not jumping, just return True)
Bring back catch when above thresh (I think its back)
index (Whats wrong with 57?)
removed unused code

"""

def gen_SCAN_ALIGNMENT(IFO, ARM):
    class SCAN_ALIGNMENT(GuardState):
        index = 57
        request = False
        redirect = False
        @fault_checker_gen(ARM)
        def main(self):

            # FIXME: Do we need this???
            pdh_ctrl(ARM, 'ON', boost=False)

            for sus in ['TMS','ETM']:
                for dof in ['P','Y']:
                    if sus == 'ETM':
                        stage = 'M0'
                    else:
                        stage = 'M1'
                    ezca.switch(':SUS-{}{}_{}_TEST_{}'.format(sus,ARM,stage,dof),'HOLD','OFF')

            ## TJ removed. We dont want this LLO dependency with their optic module
            # and it seems unnecessary since our gains are SDF controlled

            # Set test gains to match optic align gains
            #tms.copy_oagain_to_test("P", ezca)
            #tms.copy_oagain_to_test("Y", ezca)
            #etm.copy_oagain_to_test("P", ezca)
            #etm.copy_oagain_to_test("Y", ezca)

            # Channels
            self.chan = {}
            self.chan_rec = {}
            for sus in ["TMS","ETM"]:
                self.chan[sus] = {}
                self.chan_rec[sus] = {}
                if sus=="ETM":
                    stage = "M0"
                else:
                    stage = "M1"
                for dof in ["P","Y"]:
                    self.chan[sus][dof] = 'SUS-{}{}_{}_TEST_{}_'.format(sus,ARM,stage,dof)
                    self.chan_rec[sus][dof] = ':SUS-{}{}_{}_TEST_{}_'.format(sus,ARM,stage,dof)

            self.transPdChan = ':ALS-C_TR'+ARM+'_A_LF_OUTPUT'

            #etmAmpl = 1.0 #1.0    #max amplitude (amplitude of triangle wave)
            self.etmFreq1 = 0.2 #0.4		#frequency of sine waves
            self.etmFreq2 = 0.008 #0.016	#frequency of envelope (triangle wave)
            #tmsAmpl = 1.0 #1.0	#max amplitude (amplitude of triangle wave)
            self.tmsFreq1 = 0.1 #0.05		#frequency of sine waves
            self.tmsFreq2 = 0.002 #0.004	#frequency of envelope (triangle wave)
            self.f_s = 16384		#sampling frequency
            self.duration = 125

            #log("Generating waveforms")
            #etmYaw,etmPit = spiral_waveforms(etmAmpl,etmFreq1,etmFreq2,f_s,duration)
            #tmsYaw,tmsPit = spiral_waveforms(tmsAmpl,tmsFreq1,tmsFreq2,f_s,duration)
            #Tuple the waveforms
            #self.wforms = (tmsPit,tmsYaw,etmPit,etmYaw)

            #self.transPD = np.array([])
            #self.etmPitTestOut = np.array([])
            #self.etmYawTestOut = np.array([])
            #self.tmsPitTestOut = np.array([])
            #self.tmsYawTestOut = np.array([])

            #Set-up scan
            #tnow = int(gpstime.tconvert('now'))
            #self.tstart = tnow + 10
            #self.tnext = self.tstart-1
            #log("Start time is {}".format(int(self.tstart)))
            #log("Opening awg streams")

            #self.exc = []
            #for sus in ["TMS","ETM"]:
            #    for dof in ["P","Y"]:
            #        self.exc.append(awg.ArbitraryStream( IFO+":"+self.chan[sus][dof]+"EXC",rate=f_s, start=self.tstart , appinfo="two stream"))
            #for ex in self.exc:
            #    ex.open()

            #self.ii = 0

            self.tappend = 1.0
            #self.step = self.tappend*f_s #FIXME: this needs to be the number of steps (=fs)
            self.step = self.f_s

            self.set_up_stream = True
            self.append_stream = True
            self.scan_started = False
            self.savedata = False
            self.monitor = True
            self.offload_alignment = False
            self.set_from_scan = True
            self.close_stream = True
            self.assess_flashes = True
            
            self.finished = False

            # Get current tpd level
            tend = gpstime.tconvert('now') + 10
            log('Checking tpd levels (20s)')
            check_data = call_with_timeout(getdata, f'ALS-C_TR{ARM}_A_LF_OUT_DQ', 10, seconds_timeout=20)
            if check_data:
                self.max_tpd = check_data.data.max()
            else:
                log('Didnt actually get data, manually collecting...(10s)')
                tend = gpstime.tconvert('now') + 10
                transPDinit = np.array([])
                while gpstime.tconvert('now') < tend:
                    transPDinit = np.append(transPDinit,ezca[self.transPdChan])
                self.max_tpd = np.max(transPDinit)

            self.assess_flashes = True
            self.recheck_tpd = True

            self.scan_count = 0

            def exec_close_streams():
                log('Closing awg streams')
                for exnum in range(len(self.exc)):
                    self.exc[exnum].abort()
                    self.exc[exnum].close()
                self.close_stream = False
                log('awg streams closed')

            self.exec_close_streams = exec_close_streams

        # No fault check here so we can control wrap up and not leave awg open
        #@fault_checker_gen(arm)
        def run(self):

            if self.finished:
                return True
            
            # HACK! Used to wrap things up if we need to escape.
            elif ezca['GRD-ALS_{}ARM_REQUEST'.format(ARM)] != 'SCAN_ALIGNMENT':
                notify('"Gracefully" exiting Scan Alignment')
                if not self.set_up_stream:
                    self.exec_close_streams()
                    # TODO: Verify if we need to do anything with the TEST outputs/holds?
                return True
            # Wait here if we are in Fault, but should we just restore and then return FAULT?????
            #elif is_fault(ARM):
            #    notify('Arm in Fault state! Waiting...')
            #    log(ezca['ALS-'+ARM+'_LOCK_ERROR_MSG'])
            #    log(ezca['ALS-'+ARM+'_REFL_LOCK_ERROR_MSG'])
            #    log(ezca['ALS-'+ARM+'_FIBR_LOCK_ERROR_MSG'])
            #    return False


            if self.scan_count >= 4:
                log('Scan failed to find resonances')
                return True

            if self.assess_flashes:
                log('Max transPD val is {}'.format(self.max_tpd))

                if self.max_tpd >= 0.5*alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM]:
                    log('Amplitudes: ETM set to 1.0, TMS set to 2.0')
                    self.etmAmpl = 1.0
                    self.tmsAmpl = 2.0
                elif 0.5*alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM] > self.max_tpd >= 0.3*alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM]:
                    log('Amplitudes: ETM set to 3.0, TMS set to 4.0')
                    self.etmAmpl = 3.0
                    self.tmsAmpl = 4.0
                elif 0.3*alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM] > self.max_tpd >= 0.1*alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM]:
                    log('Amplitudes: ETM set to 5.0, TMS set to 6.0')
                    self.etmAmpl = 5.0
                    self.tmsAmpl = 6.0
                else:
                    log('Amplitudes: ETM set to 7.0, TMS set to 8.0')
                    self.etmAmpl = 7.0
                    self.tmsAmpl = 8.0

                self.assess_flashes = False


            if self.set_up_stream:

                log("Generating waveforms")
                etmYaw,etmPit = spiral_waveforms(self.etmAmpl,self.etmFreq1,self.etmFreq2,self.f_s,self.duration)
                tmsYaw,tmsPit = spiral_waveforms(self.tmsAmpl,self.tmsFreq1,self.tmsFreq2,self.f_s,self.duration) #TODO: add selfs above
                self.wforms = (tmsPit,tmsYaw,etmPit,etmYaw)

                self.transPD = np.array([])
                self.etmPitTestOut = np.array([])
                self.etmYawTestOut = np.array([])
                self.tmsPitTestOut = np.array([])
                self.tmsYawTestOut = np.array([])

                #Set-up scan
                tnow = int(gpstime.tconvert('now'))
                self.tstart = tnow + 10
                self.tnext = self.tstart-1
                log("Start time is {}".format(int(self.tstart)))
                log("Opening awg streams")

                self.exc = []
                for sus in ["TMS","ETM"]:
                    for dof in ["P","Y"]:
                        self.exc.append(awg.ArbitraryStream( IFO+":"+self.chan[sus][dof]+"EXC",
                                                            rate=self.f_s,
                                                            start=self.tstart ,
                                                            appinfo="two stream"))
                for ex in self.exc:
                    ex.open()

                self.ii = 0

                self.set_up_stream = False


            if self.append_stream:
                dt = self.tnext - self.tstart
                log("Time from t0: %d" % dt )
                end = min(self.ii+self.step, len(self.wforms[0]))
                #Append next part of waveforms to streams
                for exnum in range(len(self.exc)):
                    self.exc[exnum].append(self.wforms[exnum][self.ii:end])

                self.ii += self.step
                self.append_stream = False

            if (self.monitor and int(gpstime.tconvert('now')) < self.tnext):
                if not self.scan_started: 
                    if gpstime.tconvert('now')>(self.tstart-0.2):
                        log("Scanning and recording data, time is {0}".format(gpstime.tconvert('now')))
                        self.scan_started = True
                    time.sleep(0.2)
                    return
                elif ezca[self.transPdChan] > alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM]:
                    log('TMS above threshold, hold')
                    ezca.switch(':SUS-ETM{}_M0_TEST_P'.format(ARM),'HOLD','ON')
                    ezca.switch(':SUS-ETM{}_M0_TEST_Y'.format(ARM),'HOLD','ON')
                    ezca.switch(':SUS-TMS{}_M1_TEST_P'.format(ARM),'HOLD','ON')
                    ezca.switch(':SUS-TMS{}_M1_TEST_Y'.format(ARM),'HOLD','ON')
                    if self.close_stream:
                        self.exec_close_streams()
                        self.close_stream = False
                    self.monitor = False
                    self.offload_alignment = True
                    self.set_from_scan = False
                    return
                else:
                    self.transPD = np.append(self.transPD, ezca[self.transPdChan])
                    self.etmPitTestOut = np.append(self.etmPitTestOut, ezca[self.chan_rec["ETM"]["P"]+'OUTPUT'])
                    self.etmYawTestOut = np.append(self.etmYawTestOut, ezca[self.chan_rec["ETM"]["Y"]+'OUTPUT'])
                    self.tmsPitTestOut = np.append(self.tmsPitTestOut, ezca[self.chan_rec["TMS"]["P"]+'OUTPUT'])
                    self.tmsYawTestOut = np.append(self.tmsYawTestOut, ezca[self.chan_rec["TMS"]["Y"]+'OUTPUT'])
                    return
            elif (self.monitor and self.ii<len(self.wforms[0])):
                self.tnext += self.tappend
                self.append_stream = True
                return

            log("Finished scan")

            #abort and close stream
            if self.close_stream:
                self.exec_close_streams()

            # TODO: set threshold as well
            #if self.set_from_scan and np.max(self.transPD) > 0.1*alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM]:
            #    jj = np.argmax(self.transPD)
            #    offload_test_alignment(ARM, 'TMS','P',self.tmsPitTestOut[jj],tramp=3.0)
            #    offload_test_alignment(ARM, 'TMS','Y',self.tmsYawTestOut[jj],tramp=3.0)
            #    offload_test_alignment(ARM, 'ETM','P',self.etmPitTestOut[jj],tramp=3.0)
            #    offload_test_alignment(ARM, 'ETM','Y',self.etmYawTestOut[jj],tramp=3.0)
            #    self.set_from_scan = False


            # TJ TEST - seeing if getting full data will help, but it has to be after the fact
            if self.set_from_scan:
                log('Getting data during scan...')
                scan_data = call_with_timeout(getdata,
                                              [f'ALS-C_TR{ARM}_A_LF_OUT_DQ',
                                               self.chan_rec["ETM"]["P"]+'OUTPUT',
                                               self.chan_rec["ETM"]["Y"]+'OUTPUT',
                                               self.chan_rec["TMS"]["P"]+'OUTPUT',
                                               self.chan_rec["TMS"]["Y"]+'OUTPUT'],
                                              self.duration,
                                              start = self.tstart)
                # Double check that data was returned
                if scan_data:
                    if scan_data[0].data.max() > 0.01*alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM]:
                        jj = scan_data[0].data.argmax()
                        tpd_rate = scan_data[0].sample_rate
                        slow_rate = scan_data[1].sample_rate
                        slow_index = int(jj * (slow_rate/tpd_rate))
                        log('Offloading different alignment')
                        #SED CMC changed tramps to 2 because of alog 81643
                        offload_test_alignment(ARM, 'ETM', 'P', scan_data[1].data[slow_index], tramp=2.0)
                        offload_test_alignment(ARM, 'ETM', 'Y', scan_data[2].data[slow_index], tramp=2.0)
                        offload_test_alignment(ARM, 'TMS', 'P', scan_data[3].data[slow_index], tramp=2.0)
                        offload_test_alignment(ARM, 'TMS', 'Y', scan_data[4].data[slow_index], tramp=2.0)
                        self.set_from_scan = False
                else:
                    log('Didnt actually get data')
                    return

            if self.offload_alignment:
                log('Offloading TEST outputs to OPTICALIGN offsets')
                test_outputs = {}
                for sus in ['TMS','ETM']:
                    test_outputs[sus] = {}
                    if sus == 'TMS':
                        stage = 'M1'
                    else:
                        stage = 'M0'
                    for dof in ['P','Y']:
                        test_outputs[sus][dof] = ezca[':SUS-{}{}_{}_TEST_{}_OUTPUT'.format(sus,ARM,stage,dof)]

                for sus in ['TMS','ETM']:
                    for dof in ['P','Y']:
                        offload_test_alignment(ARM, sus, dof, test_outputs[sus][dof], tramp=2.0)

                time.sleep(3)
                self.offload_alignment=False

            #if self.recheck_tpd:
            #    tend = gpstime.tconvert('now') + 10
            #    transPDfinal = np.array([])
            #    while gpstime.tconvert('now') < tend:
            #        transPDfinal = np.append(transPDfinal,ezca[self.transPdChan])
            #    self.max_tpd = np.max(transPDfinal)
            #    self.recheck_tpd = False
                
                
            # Recheck_tpd TJs Version
            if self.recheck_tpd:
                tend = gpstime.tconvert('now') + 10
                log('Rechecking tpd levels (20s)')
                recheck_data = call_with_timeout(getdata, f'ALS-C_TR{ARM}_A_LF_OUT_DQ', 20, seconds_timeout=20+10)                
                if recheck_data:
                    self.max_tpd = recheck_data.data.max()
                    self.recheck_tpd = False
                else:
                    log('Didnt actually get data')
                    return
                

            if self.max_tpd < alsArmconfig.tpd_threshold['ALIGNED'][SYSTEM]:
                self.scan_count += 1
                log(f'Flashes are still too low, max trans PD val is {self.max_tpd}, scans complete=({self.scan_count}/4)')
                if self.scan_count < 4:
                    log('Rescanning, waiting for sus to settle')
                    time.sleep(5)
                self.assess_flashes = True
                self.set_up_stream = True
                self.append_stream = True
                self.scan_started = False
                self.set_from_scan = True
                self.close_stream = True
                self.recheck_tpd = True
                self.monitor = True
                return
            else:
                log(f'Max trans PD val = {self.max_tpd}. Above threshold.')


            if self.savedata:
                filedir = '/data/ASC/InitialAlignment/'+ARM+'ARM/'
                filename = filedir + 'etm{0}_tms{0}_ScanForResonance_{1}.npz'.format(ARM.lower(),int(self.tstart))
                log("Saving data to {}".format(filename))
                np.savez(filename, transPD=self.transPD, etmPitTestOut=self.etmPitTestOut, etmYawTestOut=self.etmYawTestOut, tmsPitTestOut=self.tmsPitTestOut, tmsYawTestOut=self.tmsYawTestOut)
                self.savedata = False

            # If its reached this far, then its complete. This statement reduces excess logging
            self.finished = True
            return True
        
    return SCAN_ALIGNMENT
