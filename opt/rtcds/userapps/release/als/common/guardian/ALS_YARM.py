import ALS_ARM
import ISC_GEN_STATES

global arm
arm='Y'

nominal = 'SHUTTERED'

#################################################
# STATES
#################################################

INIT                            = ALS_ARM.gen_INIT(arm)
UNLOCKED                        = ALS_ARM.gen_UNLOCKED(arm)
INCREASE_FLASHES                = ALS_ARM.gen_INCREASE_FLASHES(arm)
LOCKING                         = ALS_ARM.gen_LOCKING(arm)
INITIAL_ALIGNMENT               = ALS_ARM.gen_INITIAL_ALIGNMENT(arm)
LOCKED_W_SLOW_FEEDBACK          = ALS_ARM.gen_LOCKED_W_SLOW_FEEDBACK(arm)
ENABLE_WFS                      = ALS_ARM.gen_ENABLE_WFS_PUM(arm)
OFFLOAD_GREEN_WFS               = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('%sARM_GREEN'%arm, 20, ['ETM%s'%arm, 'ITM%s'%arm, 'TMS%s'%arm])
INITIAL_ALIGNMENT_OFFLOADED     = ALS_ARM.gen_GREEN_WFS_OFFLOADED(arm)
LOCKED_SLOW_NO_WFS              = ALS_ARM.gen_LOCKED_SLOW_NO_WFS(arm)
LOCKED_SLOW_W_GR_WFS_PUM        = ALS_ARM.gen_LOCKED_SLOW_W_GR_WFS_PUM(arm) 
LOCKED_SLOW_WFS_ETM_TMS         = ALS_ARM.gen_LOCKED_SLOW_WFS_ETM_TMS(arm)
OFFLOAD_ETM_TMS_WFS             = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('%sARM_GREEN'%arm, 20, ['ETM%s'%arm, 'TMS%s'%arm])
ETM_TMS_WFS_OFFLOADED           = ALS_ARM.gen_ETM_TMS_WFS_OFFLOADED(arm)
NO_SLOW_W_GR_WFS                = ALS_ARM.gen_NO_SLOW_W_GR_WFS(arm)
TRANSITION                      = ALS_ARM.gen_TRANSITION(arm)
LOCKED_RED                      = ALS_ARM.gen_LOCKED_RED(arm)
FAULT                           = ALS_ARM.gen_FAULT(arm)
CHECK_CRYSTAL_FREQ              = ALS_ARM.gen_CHECK_CRYSTAL_FREQ(arm)
SHUTTERED                       = ALS_ARM.gen_SHUTTERED()
CHANGE_POL                      = ALS_ARM.gen_CHANGE_POL(arm)

SCAN_ALIGNMENT                  = ALS_ARM.gen_SCAN_ALIGNMENT('H1', arm)

states_list=[INIT,
             UNLOCKED,
             LOCKING,
             INITIAL_ALIGNMENT,
             LOCKED_W_SLOW_FEEDBACK,
             OFFLOAD_GREEN_WFS,
             INITIAL_ALIGNMENT_OFFLOADED,
             LOCKED_SLOW_NO_WFS,
             LOCKED_RED,
             FAULT,
             CHECK_CRYSTAL_FREQ,
             ETM_TMS_WFS_OFFLOADED]

for state in states_list:
    state.arm = arm
del state

#################################################
# EDGES
#################################################

edges = [
    ('INIT', 'UNLOCKED'),
    ('UNLOCKED', 'INCREASE_FLASHES'),
    ('INCREASE_FLASHES', 'LOCKING'),
    ('UNLOCKED', 'SCAN_ALIGNMENT'),
    ('SCAN_ALIGNMENT', 'LOCKING'),
    ('UNLOCKED', 'LOCKING'),
    ('UNLOCKED', 'CHANGE_POL'),
    ('CHANGE_POL', 'UNLOCKED'),
    ('INIT', 'LOCKING'),
    ('LOCKING', 'LOCKED_SLOW_NO_WFS'),
    ('LOCKING', 'ENABLE_WFS'),
    ('LOCKING', 'LOCKED_SLOW_W_GR_WFS_PUM'),
    ('LOCKING', 'LOCKED_SLOW_WFS_ETM_TMS'),
    ('ENABLE_WFS', 'INITIAL_ALIGNMENT'),
    ('ENABLE_WFS', 'LOCKED_W_SLOW_FEEDBACK'),
    ('OFFLOAD_GREEN_WFS','INITIAL_ALIGNMENT_OFFLOADED'),
    ('INITIAL_ALIGNMENT_OFFLOADED','INITIAL_ALIGNMENT'),
    ('INITIAL_ALIGNMENT', 'OFFLOAD_GREEN_WFS'), 
    ('LOCKED_SLOW_WFS_ETM_TMS', 'NO_SLOW_W_GR_WFS'),
    ('LOCKED_SLOW_WFS_ETM_TMS', 'OFFLOAD_ETM_TMS_WFS'),
    ('OFFLOAD_ETM_TMS_WFS','ETM_TMS_WFS_OFFLOADED'),
    ('ETM_TMS_WFS_OFFLOADED','NO_SLOW_W_GR_WFS'),
    ('TRANSITION', 'LOCKED_RED'), 
    ('NO_SLOW_W_GR_WFS', 'TRANSITION'),
    ('TRANSITION', 'LOCKED_RED'), 
    ('LOCKED_RED', 'SHUTTERED'),
    ]
